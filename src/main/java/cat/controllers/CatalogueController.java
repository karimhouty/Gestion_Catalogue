package cat.controllers;

import java.util.List;
import java.util.Optional;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import cat.dao.IProduitRepository;
import cat.entities.Produit;

@RestController
public class CatalogueController {
	
	@Autowired
	private IProduitRepository produitRepository;
	
	@RequestMapping("/save")
	public Produit saveProduit(Produit p) {
		produitRepository.save(p);
		return p;
	}
	
	@RequestMapping("/all")
	public List<Produit> getProduits(){
		return produitRepository.findAll();
	}
	
	@RequestMapping("/produits")
	public Page<Produit> getProduitPerPage(int page){
		//return produitRepository.findAll(new PageRequest(page, 6));
		return produitRepository.findAll(PageRequest.of(page, 5));
	}
	
	@RequestMapping("/produitsParMC")
	public Page<Produit> getProduitsParMC(String mc, int page){
		return produitRepository.produitParMC("%"+mc+"%", PageRequest.of(page, 5));
	}
	
	@RequestMapping("/get")
	public Optional<Produit> getProduit(Long ref){
		return produitRepository.findById(ref);
	}
	
	@RequestMapping("/delete")
	public boolean deleteProduit(Long ref) {
		produitRepository.deleteById(ref);
		return true;
	}
	
	@RequestMapping("/update")
	public Produit update(Produit p) {
		produitRepository.saveAndFlush(p);
		return p;
	}
	
}
