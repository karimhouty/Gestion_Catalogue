var app = angular.module("MyCat", []);

app.controller("CatController", function($scope, $http){
	
	$scope.produits = [];
	$scope.motCle = "";
	$scope.pageCourante = 0;
	
	$http({
		method:'GET',
		url:'/produitsParMC?mc='+$scope.motCle+'&page='+$scope.pageCourante
	}).then(function(data){
		$scope.produits = data.data.content;
		$scope.pages = new Array(data.data.totalPages);
	});
	
	$scope.chercher = function(){
		$http({
			method:'GET',
			url:'/produitsParMC?mc='+$scope.motCle+'&page='+$scope.pageCourante
		}).then(function(data){
			$scope.produits = data.data.content;
			$scope.pages = new Array(data.data.totalPages);
		});
	};
	
	$scope.goToPage = function(p){
		$scope.pageCourante = p;
		$scope.chercher();
	}
	
	
	
});